import React, { Component } from "react";
import './uikit.css';
import './yodadmincss.css';
import './uikit-rtl.css';
import { NavLink } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserCog, faShoppingBasket, faPowerOff, faUserPlus, faBars, faFan, faImage, faCodeBranch, faFlask, faInfo, faQuestion } from '@fortawesome/free-solid-svg-icons'

class Sidebar extends Component{

    constructor(props) {
        super(props);
       
        if(localStorage.getItem('logindata') === null){
             window.location.assign("./")
         }
        
         //console.log(JSON.parse(localStorage.getItem('logindata')).id);
      }

      showAlert() {
        localStorage.setItem('logindata', null);
        window.location.assign("./")
      }

    render(){
        return <div class="sidebarleft">
            <ul>
                <li><NavLink to="/dashboard" activeClassName="active"><img alt="hhjj"  src={require('./img/dashico.png')} />Dashboard</NavLink></li>
                <li><NavLink to="/buyer" activeClassName="active"><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faUserPlus} /> Buyer Management</NavLink></li>
                <li><NavLink to="/sellerlist" activeClassName="active"><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faUserPlus} /> Seller Management</NavLink></li>
                <li><NavLink to="/product" activeClassName="active"><img alt="hhjj"  src={require('./img/myprod.png')} />My Products</NavLink></li>
                <li><NavLink to="/bannerlist" activeClassName="active"><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faImage} />Banner Management</NavLink></li>
                <li><NavLink to="/themelist" activeClassName="active"><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faFan} /> Theme Management</NavLink></li>
                <li><NavLink to="/flashsale" activeClassName="active"><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faFlask} /> Flash Sale Management</NavLink></li>
                <li><NavLink to="/brandlist" activeClassName="active"><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faCodeBranch} /> Brand Management</NavLink></li>
                <li><NavLink to="/category" activeClassName="active"><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faBars} /> Subcategory Management</NavLink></li>
                <li><NavLink to="/setting" activeClassName="active"><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faUserCog} /> Account</NavLink></li>
                <li><NavLink to="/about" activeClassName="active"><FontAwesomeIcon  style={{width:23,marginRight:10}} icon={faInfo}/>About Us</NavLink></li>
                <li><NavLink to="/faqlist" activeClassName="active"><FontAwesomeIcon  style={{width:23,marginRight:10}} icon={faQuestion}/>FAQ Management</NavLink></li>
                <li><NavLink onClick={this.showAlert}><FontAwesomeIcon style={{width:23,marginRight:10}} icon={faPowerOff} />Logout</NavLink></li>
               </ul> 
        </div>
    }
}


export default Sidebar;