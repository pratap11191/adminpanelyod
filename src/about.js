import React, { Component} from "react";
import './uikit.css';
import './yodadmincss.css';
import './uikit-rtl.css';
import './all.css';
import './fontawesome.css';
import './bootstrap.css';
import Sidebar from "./sidebar";
import Header from "./header";
import { Link } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import ImageUploader from 'react-images-upload';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import 'react-toastify/dist/ReactToastify.css';
import Select from 'react-select';
import CKEditor from "react-ckeditor-component";
const url = "https://mobuloustech.com/yodapi/api/editproductbyseller/";


class About extends Component{

	notify = () => toast("Wow so easy !");

    constructor(props) {
        super(props);
        this.updateContent = this.updateContent.bind(this);
        this.onChange = this.onChange.bind(this);
        this.afterPaste = this.afterPaste.bind(this);
        this.onBlur = this.onBlur.bind(this);
        this.state = {
            content: 'content',
        }
    
        fetch("http://mobuloustech.com/yodapi/api/getcontent/about").then((response) => response.json())
        .then((res) => { 
         //alert(res);
         if(res.status === 'FAILURE'){
            toast.error(res.message);
         } else {
        // toast.success(res.message);
         //alert(res);
         this.setState({content: res.response});
        
         //localStorage.setItem('logindata', res.sellerlogin);
          //this.props.history.push('/');
         }
         console.log(res);
        })
        .catch((error) => {
        console.log(error);
        alert('Oops, something went wrong. Please try again!');
        });
		
    }
    
    
    updateContent(newContent) {
        this.setState({
            content: newContent
        })
    }
    
    onChange(evt){
      console.log("onChange fired with event info: ", evt);
      var newContent = evt.editor.getData();
      this.setState({
        content: newContent
      })
      
      fetch("https://mobuloustech.com/yodapi/api/content",{
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          value: this.state.content,
          type: 'about',
        }),
        }).then((response) => response.json())
        .then((res) => { 
         //alert(res);
         if(res.status === 'FAILURE'){
            toast.error(res.message);
         } else {
         
            console.log(res);
         }
         console.log(res);
        })
        .catch((error) => {
        console.log(error);
        alert('Oops, something went wrong. Please try again!');
        });
    }
    
    onBlur(evt){
      console.log("onBlur event called with event info: ", evt);
    }
    
    afterPaste(evt){
      console.log("afterPaste event called with event info: ", evt);
    }
	
	  
	
	  
    render(){
        return <div class="dash-layout">
           <Header />
         
          <div class="bodylayouts-yod">	
         <Sidebar/>
            <div class="pagecontentright">
                <div class="CustomTollsabout">
                    <h2>About Us </h2>
            <CKEditor 
                activeClass="p10" 
                content={this.state.content} 
                events={{
                    "blur": this.onBlur,
                    "afterPaste": this.afterPaste,
                    "change": this.onChange
                }}
                />
    </div>
                </div>
         </div>
         </div>
    }
}


export default About;