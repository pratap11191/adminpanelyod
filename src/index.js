import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { HashRouter as Router, Route } from "react-router-dom";
import Home from "./home";
import Dashboard from "./dashboard";
import Product from "./product";
import Buyer from "./buyer";
import Forgot from "./forgot";
import Addproduct from "./addproduct";
import editproduct from "./editproduct";
import setting from "./setting";
import Category from "./category";
import header from "./header";
import orders from "./orders";
import Ordersdetails from "./orderdetails";
import Addcategory from "./addcategory";
import Editsubcatg from "./editsubcatg";
import Banneruploade from "./banneruploade";
import Bannerlist from "./bannerlist";
import Editbanner from "./editbanner";
import Addtheme from "./addtheme";
import Themelist from "./themelist";
import Addbrand from "./addbrand";
import Brandlist from "./brandlist";
import Sellerlist from "./sellerlist";
import Cuponlist from "./cuponlist";
import Addflashproduct from "./addflashproduct";
import Flashsale from "./flashsale";
import About from "./about";
import Faqlist from "./faqlist";
import Addfaq from "./addfaq";
import * as serviceWorker from './serviceWorker';


const routing = (
    <Router baseame="/build">
        <Route exact path={"/"} component={Home} />
        <Route path={"/dashboard"} component={Dashboard} />
        <Route path={"/forgot"} component={Forgot} />
        <Route path={"/product"} component={Product} />
        <Route path={"/addproduct"} component={Addproduct} />
        <Route path={"/editproduct/:userId"} component={editproduct} />
        <Route path={"/setting"} component={setting} />
        <Route path={"/header"} component={header} />
        <Route path={"/orders"} component={orders} />
        <Route path={"/ordersdetails/:userId"} component={Ordersdetails}  />
        <Route path={"/buyer"} component={Buyer} />
        <Route path={"/category"} component={Category} />
        <Route path={"/addcategory"} component={Addcategory} />
        <Route path={"/editsubcatg/:userId"} component={Editsubcatg} />
        <Route path={"/banneruploade"} component={Banneruploade} />
        <Route path={"/bannerlist"} component={Bannerlist} />
        <Route path={"/addtheme"} component={Addtheme} />
        <Route path={"/themelist"} component={Themelist} />
        <Route path={"/addbrand"} component={Addbrand} />
        <Route path={"/brandlist"} component={Brandlist} />
        <Route path={"/addflashproduct"} component={Addflashproduct} />
        <Route path={"/flashsale"} component={Flashsale} />
        <Route path={"/sellerlist"} component={Sellerlist} />
        <Route path={"/cupon"} component={Cuponlist} />
        <Route path={"/editbanner/:userId"} component={Editbanner} />
        <Route path={"/about"} component={About} />
        <Route path={"/faqlist"} component={Faqlist} />
        <Route path={"/addfaq"} component={Addfaq} />
     </Router>
  )

ReactDOM.render(routing, document.getElementById('root'));


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
