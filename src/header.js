import React, { Component } from "react";
import './uikit.css';
import './yodadmincss.css';
import './uikit-rtl.css';

class Sidebar extends Component{

    constructor(props) {
        super(props);
       
        if(localStorage.getItem('logindata') === null){
             window.location.assign("./")
         }
        
         //console.log(JSON.parse(localStorage.getItem('logindata')).id);
      }

      
    render(){
        return <header class="yodamin-hd">
        <div class="yodcontainer-dash">
        <div class="logomenumanage">
            <img alt="ok" src={require('./img/yodfull.png')} />
        </div>
        
        <div class="logge-usrdata">
            <div class="userinf-yd">
                <div class="usr-pic">
                    <img alt="hhjj" src={JSON.parse(localStorage.getItem('logindata')).image} />
                </div>
                <div class="usr-nme">
                    <h6>Welcome</h6>
                    <p class="prt-nm">{JSON.parse(localStorage.getItem('logindata')).name}</p>
                </div>
            </div>
            
            <div class="drop-opns">
                <ul>
                    <li>syrah43@gmail.com</li>
                    <li><a href="/">Logout</a></li>
                </ul>
            </div>
            
        </div>
        
        
        </div>
    </header>
    }
}


export default Sidebar;